﻿using CommonServiceLocator;
using Covid19.Managers.Providers;
using Covid19.Managers.SettingsManager;
using Covid19.Managers.UserManager;
using Covid19.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19
{
    public class AppSetup
    {
        public AppSetup()
        {
            CommonServiceLocator.ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // Services
            SimpleIoc.Default.Register<ISettingsManager, SettingsManager>();
            SimpleIoc.Default.Register<IApiProvider, ApiProvider>();
            SimpleIoc.Default.Register<IUserManager, UserManager>();
            //SimpleIoc.Default.Register<IDatabase, Database>();

            // ViewModels 
            SimpleIoc.Default.Register<GloalViewModel>();
            SimpleIoc.Default.Register<CountryReportViewModel>();
            SimpleIoc.Default.Register<HomeViewModel>();

        }
        public void ClearAll()
        {
            // SimpleIoc.Default.Unregister<ServiceCenterViewModel>(); 
        }
        public void RestAll()
        {
            //SimpleIoc.Default.Register<ServiceCenterViewModel>(); 
        }
        public GloalViewModel GloalViewModel => ServiceLocator.Current.GetInstance<GloalViewModel>();
        public CountryReportViewModel CountryReportViewModel => ServiceLocator.Current.GetInstance<CountryReportViewModel>();
        public HomeViewModel HomeViewModel => ServiceLocator.Current.GetInstance<HomeViewModel>();
    }
}
