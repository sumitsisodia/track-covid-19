﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19.Models
{
    public partial class AboutTeamModel
    {
        public string CardsTopImage { get; set; }
        public List<Aboutteam> AboutteamData { get; set; }
    }

    public partial class Aboutteam
    { 
        public string FirstName { get; set; }  
        public string LastName { get; set; }  
        public string City { get; set; } 
        public string Designation { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public List<Sociallinktask> linkSocial { get; set; }
    }

    public partial class Sociallinktask
    {
        public string LinkedLink { get; set; } 
        public string YtLink { get; set; } 
        public string InstaLink { get; set; } 
        public string Csharpcorner { get; set; } 
      //  public string FbLink { get; set; } 
       
       // public string OtherLink { get; set; } 
        public string BloggerLink { get; set; } 
    }
} 