﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Covid19.Models
{
    public class CovidCurrentCountryResponse:BaseResponse
    {
        public CountryData data { get; set; }
        public bool _cacheHit { get; set; }
    }
    public class Coordinates
    {
        public double? latitude { get; set; }
        public double? longitude { get; set; }
    }

    public class Today
    {
        public int deaths { get; set; }
        public int confirmed { get; set; }
    }

    public class Calculated
    {
        public double? death_rate { get; set; }
        public double? recovery_rate { get; set; }
        public double? recovered_vs_death_ratio { get; set; }
        public double? cases_per_million_population { get; set; }
    }

    public class LatestData
    {
        public int deaths { get; set; }
        public int confirmed { get; set; }
        public int recovered { get; set; }
        public int critical { get; set; }
        public Calculated calculated { get; set; }
    }

    public class Timeline
    {
        public DateTime updated_at { get; set; }
        public string date { get; set; }
        public int deaths { get; set; }
        public int confirmed { get; set; }
        public int active { get; set; }
        public int recovered { get; set; }
        public int new_confirmed { get; set; }
        public int new_recovered { get; set; }
        public int new_deaths { get; set; }
        public bool is_in_progress { get; set; }
    }

    public class CountryData
    {
        public Coordinates coordinates { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int? population { get; set; }
        public DateTime updated_at { get; set; }
        public Today today { get; set; }
        public LatestData latest_data { get; set; }
        public ObservableCollection<Timeline> timeline { get; set; }
    }

   
}
