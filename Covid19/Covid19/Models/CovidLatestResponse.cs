﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Covid19.Models
{ 
    public class CovidLatestResponse:BaseResponse
    {
        public ObservableCollection<CovidData> data { get; set; }
        public bool _cacheHit { get; set; }
    } 
    
    public class CovidData
    {
        public DateTime updated_at { get; set; }
        public string date { get; set; }
        public int deaths { get; set; }
        public int confirmed { get; set; }
        public int recovered { get; set; }
        public int active { get; set; }
        public int new_confirmed { get; set; }
        public int new_recovered { get; set; }
        public int new_deaths { get; set; }
        public bool is_in_progress { get; set; }
    }
}
