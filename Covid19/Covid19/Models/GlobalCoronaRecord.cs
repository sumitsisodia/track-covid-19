﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19.Models
{
   public class GlobalCoronaRecord:BaseResponse
    {
        public int cases { get; set; }
        public int deaths { get; set; }
        public int recovered { get; set; }
    }
}
