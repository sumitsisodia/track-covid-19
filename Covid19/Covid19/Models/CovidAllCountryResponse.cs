﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Covid19.Models
{
   public class CovidAllCountryResponse:BaseResponse
    {
        [JsonProperty("data")]
        public ObservableCollection<CountryListData> CountryData { get; set; }
        public bool _cacheHit { get; set; }
    }
     

    public class CountryListData
    {
        public Coordinates coordinates { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int? population { get; set; }
        public DateTime updated_at { get; set; }
        public Today today { get; set; }
        public LatestData latest_data { get; set; }
    } 
}
