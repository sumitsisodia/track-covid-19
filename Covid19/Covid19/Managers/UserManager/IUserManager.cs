﻿using Covid19.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.Managers.UserManager
{
    public interface IUserManager
    {
        #region All Response
        CovidLatestResponse CovidLatestResponse  { get; }
        CovidCurrentCountryResponse CovidCountryResponse { get; }
        CovidAllCountryResponse CovidAllCountryResponse { get; }
        GlobalCoronaRecord GlobalCoronaRecord { get; } 
        #endregion





        #region All Api
        Task CovidLatestList(Action success, Action<BaseResponse> failed);
        Task CovidCurrentCountry(Action success, Action<BaseResponse> failed);
        Task CovidAllCountryList(Action success, Action<BaseResponse> failed);
        Task GlobalCovidRecord(Action success, Action<BaseResponse> failed); 
        #endregion
    }
}
