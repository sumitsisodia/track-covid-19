﻿using Covid19.Managers.Providers;
using Covid19.Managers.SettingsManager;
using Covid19.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.Managers.UserManager
{
    public class UserManager : IUserManager
    {
        #region Constructor
        public UserManager(IApiProvider apiProvider, ISettingsManager settingsManager)
        {
            _apiProvider = apiProvider;
            _settingsManager = settingsManager;
        }
        #endregion

        #region Connectivity
        public bool IsConnected()
        {
            // var networkConnection = Xamarin.Forms.DependencyService.Get<Services.IMediaService>().CheckNewworkConnectivity().Result; //CrossConnectivity.Current.IsConnected;
            return true;// networkConnection;
        }
        #endregion

        #region Property
        private readonly IApiProvider _apiProvider;
        private readonly ISettingsManager _settingsManager;
        #endregion

        #region Response

        private CovidLatestResponse covidlatestresponse { get; set; }
        public CovidLatestResponse CovidLatestResponse => covidlatestresponse;

        private CovidCurrentCountryResponse covidcountryresponse { get; set; }
        public CovidCurrentCountryResponse CovidCountryResponse => covidcountryresponse;

        private CovidAllCountryResponse covidallcountryresponse { get; set; }
        public CovidAllCountryResponse CovidAllCountryResponse => covidallcountryresponse;
        private GlobalCoronaRecord globalcoronarecord { get; set; }
        public GlobalCoronaRecord GlobalCoronaRecord => globalcoronarecord;
        private GlobalCountryResponse globalcountryresponse { get; set; }
        public GlobalCountryResponse GlobalCountryResponse => globalcountryresponse;

        //
       
        #endregion

        #region Header
        public Dictionary<string, string> GetHeaders()
        {
            //Authorization: Bearer ZEmEQtlZ8uSmAjDSTkSPiCkRPesno0GB5
            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("Authorization", $"{"Bearer "}ZEmEQtlZ8uSmAjDSTkSPiCkRPesno0GB5");
            // header.Add("auth-secret", "Ak12mr27Xwg@d89ul");
            return header;
        }
        #endregion

        #region Api Calling

        public async  Task CovidLatestList(Action success, Action<BaseResponse> failed)
        {
         
            bool IsNetwork = IsConnected();
            if (IsNetwork)
            {
                try
                {
                    var url = string.Format("{0}timeline", _settingsManager.ApiHost);
                    var result = await _apiProvider.Get<CovidLatestResponse>(url);
                    if (result.IsSuccessful)
                    {
                        if (success != null)
                        {
                            covidlatestresponse = result.Result; 
                            success.Invoke();
                        }
                        else
                        {
                            failed.Invoke(result.Result);
                        }
                    }
                    else
                    {
                        failed.Invoke(result.Result);
                    }

                }
                catch (Exception exc)
                {

                }
            }
            else
            {
                BaseResponse error = new BaseResponse { ErrorMessage = "Please check your internet connection!" };
                failed.Invoke(error);
            }
        }

        public async Task CovidCurrentCountry(Action success, Action<BaseResponse> failed)
        {

            bool IsNetwork = IsConnected();
            if (IsNetwork)
            {
                try
                {
                    var url = string.Format("{0}countries/IN", _settingsManager.ApiHost);
                    var result = await _apiProvider.Get<CovidCurrentCountryResponse>(url);
                    if (result.IsSuccessful)
                    {
                        if (success != null)
                        {
                            covidcountryresponse = result.Result;
                            success.Invoke();
                        }
                        else
                        {
                            failed.Invoke(result.Result);
                        }
                    }
                    else
                    {
                        failed.Invoke(result.Result);
                    }

                }
                catch (Exception exc)
                {

                }
            }
            else
            {
                BaseResponse error = new BaseResponse { ErrorMessage = "Please check your internet connection!" };
                failed.Invoke(error);
            }
        }

        public async Task CovidAllCountryList(Action success, Action<BaseResponse> failed)
        {

            bool IsNetwork = IsConnected();
            if (IsNetwork)
            {
                try
                {
                    var url = string.Format("{0}countries", _settingsManager.ApiHost);
                    var result = await _apiProvider.Get<CovidAllCountryResponse>(url);
                    if (result.IsSuccessful)
                    {
                        if (success != null)
                        {
                            covidallcountryresponse= result.Result;
                            success.Invoke();
                        }
                        else
                        {
                            failed.Invoke(result.Result);
                        }
                    }
                    else
                    {
                        failed.Invoke(result.Result);
                    }

                }
                catch (Exception exc)
                {

                }
            }
            else
            {
                BaseResponse error = new BaseResponse { ErrorMessage = "Please check your internet connection!" };
                failed.Invoke(error);
            }
        }

        public async Task GlobalCovidRecord(Action success, Action<BaseResponse> failed)
        {

            bool IsNetwork = IsConnected();
            if (IsNetwork)
            {
                try
                {
                    var url = string.Format("https://coronavirus-19-api.herokuapp.com/all");
                    var result = await _apiProvider.Get<GlobalCoronaRecord>(url);
                    if (result.IsSuccessful)
                    {
                        if (success != null)
                        {
                            globalcoronarecord= result.Result;
                            success.Invoke();
                        }
                        else
                        {
                            failed.Invoke(result.Result);
                        }
                    }
                    else
                    {
                        failed.Invoke(result.Result);
                    } 
                }
                catch (Exception exc)
                {

                }
            }
            else
            {
                BaseResponse error = new BaseResponse { ErrorMessage = "Please check your internet connection!" };
                failed.Invoke(error);
            }
        }

      


        #endregion
    }
}
