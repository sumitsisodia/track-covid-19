﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19.Managers.SettingsManager
{
    public interface ISettingsManager
    {
        string ApiHost { get; }
        string Token { get; }
    }
}
