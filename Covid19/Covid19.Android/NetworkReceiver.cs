﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using static Xamarin.Essentials.Permissions;

namespace Covid19.Droid
{
    [BroadcastReceiver(Enabled = true, Exported = true)]
    //[IntentFilter(new string[] {  "com.connectingdotss.covid19.MyInternetBroadcastReceiver", "android.net.wifi.WIFI_STATE_CHANGED", "android.net.conn.CONNECTIVITY_CHANGE" })]
    //[Android.Runtime.Preserve(AllMembers = true)]
    public class NetworkReceiver : BroadcastReceiver 
    {
  

        public override void OnReceive(Context context, Intent intent)
        {
            try
            {
                var connectivityManager = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
                var wifi = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi);
                var mobile = connectivityManager.GetNetworkInfo(ConnectivityType.Mobile);
                //   var noConnection = wifi == NetworkInfo.State.Connected;
                if (((null != wifi) && (wifi.IsAvailable) || ((null != mobile) && (mobile.IsAvailable))))
                {
                    if (wifi.IsConnected|| mobile.IsConnected)
                    {
                        App.AppSetup.GloalViewModel.GetGlobalCoronaDataCommand.Execute(null);
                        Toast.MakeText(Android.App.Application.Context, "Wifi or mobile Internet is Available", ToastLength.Long).Show(); 
                    }
                    //else if(mobile.IsConnected) 
                    //    Toast.MakeText(Android.App.Application.Context, "Mobile Internet is Available", ToastLength.Long).Show(); 
                    else 
                        Toast.MakeText(Android.App.Application.Context, "Internet is not Available", ToastLength.Long).Show(); 
                }
                else
                {
                    Android.Widget.Toast.MakeText(Android.App.Application.Context, "Network is not Available", ToastLength.Long).Show();
                }
              

                //ConnectivityManager connectivity = (ConnectivityManager)
                //context.GetSystemService(Context.ConnectivityService);
                //if (connectivity != null)
                //{
                //    NetworkInfo[] info = connectivity.GetAllNetworkInfo();
                //    foreach (NetworkInfo nwork in info)
                //    {
                //        if (nwork.GetState() == NetworkInfo.State.Connected)
                //        {

                //           // ConnectionDetected();//Execute your fonction here
                //            break;

                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {

             }
        } 

    }
    

}